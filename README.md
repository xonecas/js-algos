Algorithms study.
=================

I wrote this way back, and over time a lot of people mentioned it as a very
helpful reference. Since I got that feedback I felt it was time to update it
and bring the code to more modern standards. So I installed a bunch of packages
to pretend that Javascript is already a mature language, just remember we still
don't have real integers.

Build the project with:

  - `npm run build`

Run the test with

  - `npm run test`

TODO
====

 * Continue to improve the syntax to use ES6 latest and greatest stuff
 * Look into using YARN instead of NPM because it's a better tool (I've been told)
 * Look into a better way to write simple tests, maybe this http://www.node-tap.org/
